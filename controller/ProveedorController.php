<?php
class ProveedorController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
        
    }

    public function index()
    {
    
    }

    public function deudas()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"] > 3){
            $cartera = new Cartera($this->adapter);
            $carteras = $cartera->getCreditoProveedorAll();
        $pago = 0;
        $por_pagar = 0;
        $vencidas = 0;
        $total=0;
        $color = 'danger'; 
        foreach ($carteras as $calculo) {
           $pago += $calculo->total_pago;
           $por_pagar += ($calculo->deuda_total - $calculo->total_pago);
           $total += $calculo->deuda_total;
            if($calculo->fecha_pago < date("Y-m-d")){
                $vencidas +=($calculo->deuda_total - $calculo->total_pago);
            }
        } 
        $porcentaje_vencido = bcdiv(($vencidas / $total)*100,'1',1);
        $porcentaje_pago = bcdiv(($pago / $total)*100,'1',1);

        if($porcentaje_pago < 30.0){
            $color = "danger";
        }elseif($porcentaje_pago > 30.1 && $porcentaje_pago < 80.0){
            $color = "primary";
        }elseif($porcentaje_pago > 80.1){
            $color = "success";
        }
        

        
        $this->frameview("proveedor/deudas/deudas",array(
            "cartera"=>$carteras,
            "deuda_total"=>$total,
            "deuda_pagada"=>$pago,
            "vencidas"=> $vencidas,
            "prcentaje_pago"=>$porcentaje_pago,
            "porcentaje_vencido"=>$porcentaje_vencido,
            "color"=>$color

        ));
        }
    }

    public function pagar_deuda()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"] > 3){
            if(isset($_GET["data"]) && !empty($_GET["data"]) && $_GET["data"] >0){
                $idcredito = $_GET["data"];

                $cartera = new Cartera($this->adapter);
                $credito= $cartera->getCreditoProveedorById($idcredito);

                $pagos = $cartera->getPagoCarteraProveedor($idcredito);

                $this->frameview("proveedor/deudas/pagarDeuda",array(
                    "credito"=>$credito,
                    "pagos"=>$pagos,
                ));


            }else{
                echo "Forbidden Gateway";
            }
        }else{
            echo "Forbidden Gateway";
        }
    }

    public function generar_pago_proveedor()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"]> 1){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                //models
                $retencion = new Retenciones($this->adapter);
                $cartera = new Cartera($this->adapter);
                $comprobante = new Comprobante($this->adapter);
                $comprobantes = $comprobante->getComprobanteAll();
                //forma_pago en una variable
                $forma_pago = $_GET["data"];
                $attr= "c_cobrar";
                $param="1";
                //idcredito en una variable
                $idcredito = $_POST["id_credito_proveedor"];
                $pos = "proveedor";
                //obtener informacion de este credito
                
                $credito = $cartera->getCreditoProveedorById($idcredito);
                //traer lista de retenciones
                
                $retenciones = $retencion->getAll();

                //funcion de opciones de prcio rapidas
                    foreach ($credito as $data) {}
                    $total = $data->deuda_total-$data->total_pago;
                    $idcartera = $data->idcredito_proveedor;
                    //cantidad de caracteres
                    $listPrice = [];
                    $caracteres = strlen($total);
                    $max = substr($total,0,1) +1;
                    $aprox = substr($total,0,2) +1;
                    for($i=0;$i<$caracteres-1;$i++){
                        $max .="0";
                    }

                    for($i=0;$i<$caracteres-2;$i++){
                        $aprox .="0";
                    }

                    $listPrice[] = $total;
                    $listPrice[] = $aprox;
                    $listPrice[] = $max;
                
                switch ($forma_pago) {
                    case '1':
                        # vista para pago en efectivo

                        $this->frameview("cartera/efectivo/efectivo",array(
                            "comprobantes"=>$comprobantes,
                            "credito"=>$credito,
                            "listPrice"=>$listPrice,
                            "retenciones"=>$retenciones,
                            "idcredito" =>$idcartera,
                            "pos"=>$pos,
                            "attr"=>$attr,
                            "param"=>$param
                        ));
                        break;
                    case '2':
                        # code...
                        break;
                    case '3':
                        # code...
                        break;
                    
                    default:
                        # code...
                        break;
                }

            }
        }else{
            echo "No tienes permisos";
        }
    }

    public function calcularCartera()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"]> 1){
            if(isset($_POST["pago"])) {
                $msg="Por pagar";
                $attr = true;
                $color="text-success";
                $credito = $_POST["credito"];
                $idretencion = $_POST["retencion"];
                $status = false;
                $pago = (isset($_POST["pago"]) && $_POST["pago"]>0 || $_POST["pago"] !="")?$_POST["pago"]:0;
                //llamar clases para calcular el total
                $retenciones = new Retenciones($this->adapter);
                $cartera = new Cartera($this->adapter);
                //llamar funciones para calcular total
                $credito = $cartera->getCreditoProveedorById($credito);
                $retencion = $retenciones->getRetencionesById($idretencion);
                //obteniendo resultados en variables
                foreach ($retencion as $retencion) {}
                foreach ($credito as $credito) {}
                //
                $deuda = $credito->deuda_total - $credito->total_pago;
                if($idretencion > 0){
                 if($retencion){
                    $total = $deuda / (($retencion->re_porcentaje/100)+1) - $pago;
                    $reteinfo = $retencion->re_porcentaje;
                 }
                }else{
                    $reteinfo = false;
                    $total =$deuda - $pago;
                }

                if($deuda <= $pago){
                    $msg="Cambio";
                    $color="text-danger";
                }

                if($pago > 0){
                    $attr =false;
                }

                if($pago <= 0 || $pago > $deuda && $credito->contabilidad == 1){
                    $status = false;
                }else{
                    $status = true;
                }

                echo json_encode(array("total"=>abs($total),"msg"=>$msg,"color"=>$color,"attr"=>$attr,"status"=>$status));
            }
        }
    }

    public function pago_autorizado()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"]> 1 && $_POST["pago"] > 0){
            //almacenando variables
            $pago = $_POST["pago"];
            $idcredito = $_POST["idcredito"];
            $idretencion = $_POST["retenciones"];
            $tipo_pago = $_POST["tipo_pago"];
            $comprobanteid = (isset($_POST["comprobante"]) && !empty($_POST["comprobante"]))?$_POST["comprobante"]:false;
            $cuenta_pago = (isset($_POST["cuenta_pago"]) && !empty($_POST["cuenta_pago"]))?$_POST["cuenta_pago"]:"";
            //llamar clases
            $retenciones = new Retenciones($this->adapter);
            $cartera = new Cartera($this->adapter);
            $ingresocontable = new IngresoContable($this->adapter);
            $detalleingresocontable = new DetalleIngresoContable($this->adapter);
            $comprobantecontable= new ComprobanteContable($this->adapter);
            $detallecomprobantecontable = new DetalleComprobanteContable($this->adapter);
            $comprobantes = new Comprobante($this->adapter);
            $puc = new PUC($this->adapter);

            //llamar funciones para calcular total
            $credito = $cartera->getCreditoProveedorById($idcredito);
            $retencion = $retenciones->getRetencionesById($idretencion);
            $array = explode(" - ", $cuenta_pago);
            $i =0;
            foreach ($array as $search) {$getcuenta = $puc->getPucById($array[$i]);
                //si se encontro algo en proveedores lo retorna
            foreach ($getcuenta as $dacuenta) {}
            $i++;
            }
            //ahora cuenta_pago va a ser la cuenta recuperada
            $cuenta_pago = $dacuenta->idcodigo;
            //obteniendo resultados en variables
            foreach ($retencion as $retencion) {}
            foreach ($credito as $credito) {}
            $cuenta_proveedor = "";
            $cuenta_nombre = "";
            $cuenta_proveedores=0;
            $cuenta_pago_credito=0;
            $nombre_cuenta_pago_credito ="";
            $deuda_actual = $credito->deuda_total - $credito->total_pago;
            $setcomprobante=0;

            //saber si el credito esta enlazado a una factura contable
            if($credito->contabilidad == 1){

                //recuperar comprobante contable donde se almacenara
                $comprobante = $comprobantes->getComprobanteById($comprobanteid);
                foreach($comprobante as $datacomprobante){}
                $ingreso = $comprobantecontable->getComprobanteById($credito->idingreso);
                if($ingreso){
                    foreach ($ingreso as $data) {}
                    //ahora obtener detalle de ese ingreso contable
                    $detalleingreso = $detallecomprobantecontable->getArticulosByComprobante($credito->idingreso);
                    if($detalleingreso){
                        foreach ($detalleingreso as $datadetalle) {
                            $getpuc = $puc->getPucById($datadetalle->dcc_cta_item_det);
                            if($getpuc){
                                foreach ($getpuc as $datapuc) {
                                    if($datapuc->centro_costos){
                                        $cuenta_proveedor = $datapuc->idcodigo;
                                        $cuenta_nombre = $datapuc->tipo_codigo;
                                    }else{}

                                    ////obtener cuenta de pago en esta compra
                                    if($datapuc->centro_costos && $datadetalle->dcc_valor_item > 0){
                                        $cuenta_proveedores = $datapuc->idcodigo;
                                        $nombre_cuenta_proveedores =$datapuc->tipo_codigo;
                                    }

                                }

                            }
                        }
                    }

                    $tercero = $data->documento_proveedor;
                    //ingreso de cuenta con la que se paga al proveedor
                    $pucpago = $puc->getPucById($cuenta_pago);
                    if($pucpago){
                        foreach ($pucpago as $pucpago) {}
                        if($pucpago->idcodigo && $pucpago->movimiento){
                            $cuenta_pago_credito = $pucpago->idcodigo;
                            $nombre_cuenta_pago_credito = $pucpago->tipo_codigo;
                        }
                    }

                }
            }

            if($idretencion >0){
                if($retencion){
                    if($pago <= $deuda_actual){
                    $precio_retenido = $pago / (($retencion->re_porcentaje/100)+1) - $pago;
                    $pago_parcial = $pago;
                    $deuda = $deuda_actual - $pago_parcial;
                    }else{
                    $precio_retenido =$deuda_actual / (($retencion->re_porcentaje/100)+1) - $deuda_actual;
                    $pago_parcial = $deuda_actual;
                    $deuda = 0;
                    }
                }
            }else{
                if($pago <= $deuda_actual){
                    $pago_parcial = $pago;
                    $deuda = $deuda_actual - $pago_parcial;
                    $precio_retenido =0;
                }else{
                    $pago_parcial = $deuda_actual;
                    $deuda = 0;
                    $precio_retenido =0;
                }
            } 

                

                if($cuenta_proveedores){
                    $comprobantecontable->setCc_idusuario($_SESSION["usr_uid"]);
                    $comprobantecontable->setCc_idproveedor($data->cc_idproveedor);
                    $comprobantecontable->setCc_tipo_comprobante("C");
                    $comprobantecontable->setCc_id_forma_pago($data->cc_id_forma_pago);
                    $comprobantecontable->setCc_id_tipo_cpte($datacomprobante->iddetalle_documento_sucursal);
                    $comprobantecontable->setCc_num_cpte($datacomprobante->ultima_serie);
                    $comprobantecontable->setCc_cons_cpte(zero_fill($datacomprobante->ultimo_numero+1,8));
                    $comprobantecontable->setCc_det_fact_prov($data->cc_num_cpte."".$data->cc_cons_cpte);
                    $comprobantecontable->setCc_fecha_cpte(date("Y-m-d"));
                    $comprobantecontable->setCc_fecha_final_cpte("0000-00-00");
                    $comprobantecontable->setCc_nit_cpte($data->cc_nit_cpte);
                    $comprobantecontable->setCc_dig_verifi(0);
                    $comprobantecontable->setCc_ccos_cpte($_SESSION["idsucursal"]);
                    $comprobantecontable->setCc_fp_cpte(0);
                    $comprobantecontable->setCc_estado("A");
                    $comprobantecontable->setCc_log_reg($_SESSION['usr_uid']."_".date("Y-m-d")."_".date("h:i:s"));
                    $addComprobante = $comprobantecontable->saveComprobanteContable();

                    //usar comprobante
                    $usarcomprobante = $comprobantes->usarComprobante($comprobanteid);

                    //crear comprobante contable
                    $detallecomprobantecontable->setDcc_id_trans($addComprobante);
                    $detallecomprobantecontable->setDcc_seq_detalle(0);
                    $detallecomprobantecontable->setDcc_cta_item_det($cuenta_proveedores);
                    $detallecomprobantecontable->setDcc_det_item_det($nombre_cuenta_proveedores);
                    $detallecomprobantecontable->setDcc_cod_art(0);
                    $detallecomprobantecontable->setDcc_cant_item_det(1);
                    $detallecomprobantecontable->setDcc_ter_item_det($tercero);
                    $detallecomprobantecontable->setDcc_ccos_item_det($data->cc_ccos_cpte);
                    $detallecomprobantecontable->setDcc_d_c_item_det("C");
                    $detallecomprobantecontable->setDcc_valor_item($pago_parcial);
                    $detallecomprobantecontable->setDcc_base_imp_item(0);
                    $detallecomprobantecontable->setDcc_base_ret_item($precio_retenido);
                    $detallecomprobantecontable->setDcc_fecha_vcto_item($data->cc_fecha_final_cpte);
                    $detallecomprobantecontable->setDcc_dato_fact_prove($data->cc_num_cpte."".$data->cc_cons_cpte);
                    $addItem=$detallecomprobantecontable->addArticulos();

                    //imprimir segunda cuenta
                    if($cuenta_pago_credito){
                    $detallecomprobantecontable->setDcc_id_trans($addComprobante);
                    $detallecomprobantecontable->setDcc_seq_detalle(0);
                    $detallecomprobantecontable->setDcc_cta_item_det($cuenta_pago_credito);
                    $detallecomprobantecontable->setDcc_det_item_det($nombre_cuenta_pago_credito);
                    $detallecomprobantecontable->setDcc_cod_art(0);
                    $detallecomprobantecontable->setDcc_cant_item_det(1);
                    $detallecomprobantecontable->setDcc_ter_item_det($tercero);
                    $detallecomprobantecontable->setDcc_ccos_item_det($data->cc_ccos_cpte);
                    $detallecomprobantecontable->setDcc_d_c_item_det("D");
                    $detallecomprobantecontable->setDcc_valor_item($pago_parcial);
                    $detallecomprobantecontable->setDcc_base_imp_item(0);
                    $detallecomprobantecontable->setDcc_base_ret_item($precio_retenido);
                    $detallecomprobantecontable->setDcc_fecha_vcto_item("");
                    $detallecomprobantecontable->setDcc_dato_fact_prove("");
                    $addItem=$detallecomprobantecontable->addArticulos();

                    $creditoid = $addComprobante;
                    $setcomprobante = $addComprobante;
                    }else{
                    $creditoid= $idcredito;
                    $setcomprobante = 0;
                    }  

                }

                $cartera->setIdcredito_proveedor($idcredito);
                $cartera->setIdcomprobante($setcomprobante);
                $cartera->setCuenta_contable($cuenta_proveedores);
                $cartera->setCuenta_contable_pago($cuenta_pago);
                $cartera->setPago_parcial($pago_parcial);
                $cartera->setDeuda_parcial($deuda);
                $cartera->setMonto($pago);
                $cartera->setTipo_pago($tipo_pago);
                $cartera->setRetencion($precio_retenido);
                $cartera->setEstado(1);
                if($pago_parcial>0){
                $pago = $cartera->generar_pago_proveedor();
                }

                if($cuenta_proveedores){
                    echo json_encode(array("success"=>"file/comprobantes/$addComprobante"));
                }else{
                    echo json_encode(array("success"=>"file/cartera/cliente/$idcredito"));
                }


        }else{
            echo "No se puede realizar este pago";
        }
    }
}
