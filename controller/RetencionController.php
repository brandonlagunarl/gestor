<?php
class RetencionController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
        
    }

    public function index()
    {
        
    }

    public function addRetencionToCart()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >0){
            if(isset($_POST["data"]) && !empty($_POST["data"])){
                $carro = new ColaIngreso($this->adapter);
                $colaretencion = new ColaRetencion($this->adapter);
                $getCart = $carro->getCart();
                foreach($getCart as $getCart){}
                $colaretencion->setCdr_ci_id($getCart->ci_id);
                $colaretencion->setCdr_re_id($_POST["data"]);
                $colaretencion->setCdr_contabilidad(0);
                $colaretencion->addRetencion();
                echo json_encode("ingresado");

            }else{}
        }else{}
    }
}