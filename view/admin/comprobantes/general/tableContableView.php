<?php 
$i=1;
foreach ($detalle as $detalle) { 
        if($detalle->estado == "A" && $detalle->debito ==$detalle->credito){
            $estado = "fa-check-circle";
            $color = "text-success";
            $message ="Aceptado";
        }elseif($detalle->estado =="A" && $detalle->debito != $detalle->credito || $detalle->debito !=""){
            $estado = "fa-exclamation-circle";
            $color = "text-warning";
            $message ="Alerta";
        }
        else{
            $estado = "fa-times-circle";
            $color = "text-dange";
            $message ="Cancelado";
        }
    ?>
    <tr>
        <td><?=$i?></td>
        <td><?=$detalle->tipo_comprobante." ".$detalle->serie_comprobante."".zero_fill($detalle->num_comprobante,8)?></td>
        <td><?=$detalle->nombre_tercero?></td>
        <td><?=$detalle->idsucursal?></td>
        <td><?=$detalle->fecha?></td>
        <td><?=number_format($detalle->debito,2,'.',',')?></td>
        <td><?=number_format($detalle->credito,2,'.',',')?></td>
        <td><i class="fas <?=$estado." "?> <?=$color?>" data-toggle="tooltip-primary" data-placement="top" title="Estado <?=$message?>"></i></td>
        <td>
            <a href="#comprobantes/edit/<?=$detalle->cc_id_transa?>"><i class="fas fa-pencil-alt text-warning"></i></a>
            <?php if($detalle->estado == 'A' ){?>
                <i class="fas fa-trash text-danger" data-toggle="modal" data-target="#modaldemo2" onclick="sendIdModal('comprobantes/delete/<?=$detalle->cc_id_transa?>')"></i>&nbsp;
            <?php }?>
            <a href="#file/comprobantes/<?=$detalle->cc_id_transa?>" target="" ><i class="fas fa-print text-info"></i></a>&nbsp;
            <a href="#file/comprobantes/<?=$detalle->cc_id_transa?>/standard" target="" ><i class="fas fa-print text-info"></i></a>
        </td>
    </tr>
<?php $i++;}?>