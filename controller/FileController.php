<?php
class FileController extends Controladorbase{

    private $adapter;
    private $conectar;

    public function __construct() {
       parent::__construct();

       $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function index()
    {
       
    }

    public function venta()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"] > 2){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                
                $idventa = $_GET["data"];
                //configuracion para el modo de visualizacion
                $view = (isset($_GET["s"])&&!empty($_GET["s"]))?$_GET["s"]:"";
                $file_height = (isset($view))?"100%":"92.4%";
                $conf_print = (isset($_GET["t"]) && !empty($_GET["t"]))?$_GET["t"]:"";
                //recuperando la venta por id
                $ventas = new Ventas($this->adapter);
                $venta = $ventas->getVentaById($idventa);
                if($venta){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($venta as $data){}
                //ecuperando la sucursal por factura de venta
                $sucursales = new Sucursal($this->adapter);
                $sucursal = $sucursales->getSucursalById($data->idsucursal);
               
                //traer la vista del tipo de impresion que se aplica a este comprobante
                $funcion = $data->pri_conf."_venta";
                //configuracion solo para desarrollo
                //$location = "http://127.0.0.1/app";
                $location = LOCATION_CLIENT;
                

              $this->frameview("file/pdf/".$data->pri_nombre,array(
                   "file_height"=>$file_height,
                   "conf_print"=>$conf_print,
                   "venta"=>$venta,
                   "sucursal"=>$sucursal,
                   "funcion"=>$funcion,
                   "id"=>$idventa,
                   "url"=>$location
               ));
                }else{
                    echo "Factura no disponible";
                }
            }else{
                
            }
        }
    }

    public function caja()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] > 2){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                
                $idreporte = $_GET["data"];
                $view = (isset($_GET["s"])&&!empty($_GET["s"]))?$_GET["s"]:"";
                $file_height = (isset($view))?"100%":"92.4%";
                $conf_print = (isset($_GET["t"]) && !empty($_GET["t"]))?$_GET["t"]:"";

                //recuperando la venta por id
                $caja = new ReporteCaja($this->adapter);
                $reporte = $caja->getReporteById($idreporte);
                if($reporte){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($reporte as $data){}
                //ecuperando la sucursal por factura de venta
                $sucursales = new Sucursal($this->adapter);
                $sucursal = $sucursales->getSucursalById($data->rc_idsucursal);
               
                //traer la vista del tipo de impresion que se aplica a este comprobante
                $funcion = "cierre_caja";
                //configuracion solo para desarrollo
                //$location = "http://127.0.0.1/app";
                $location = LOCATION_CLIENT;
                

              $this->frameview("file/pdf/reporteCaja",array(
                    "file_height"=>$file_height,
                    "con_print"=>$conf_print,
                   "sucursal"=>$sucursal,
                   "funcion"=>$funcion,
                   "id"=>$idreporte,
                   "url"=>$location
               ));
                }else{
                    echo "Reporte no disponible";
                }
            }else{
                
            }
        }
    }

    public function impuestos(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] > 3){
            if(isset($_GET["data"]) && isset($_GET["s"])){

                ##### CONFIGURACION ####
                $start_date = $_GET["data"];
                $end_date = $_GET["s"];
                $funcion = "reporte_impuestos";
                $url=LOCATION_CLIENT;
                $redirect ="impuestos/reporte";
                $file_height ="92.4%";
                $state = false;
                ##### CONFIGURACION ####
                
                //Models
                $venta = new Ventas($this->adapter);
                $ventacontable = new VentaContable($this->adapter);
                $detalleventa = new DetalleVenta($this->adapter);
                $detalleventacontable = new DetalleVentaContable($this->adapter);

                //Functions
                $ventas = $venta->reporte_detallado($start_date,$end_date);
                foreach($ventas as $ventas){}
                if($ventas){$state=true;}

                if($state){
                    $this->frameview("file/pdf/impuestos",array(
                        "start_date"=>$start_date,
                        "end_date"=>$end_date,
                        "funcion"=>$funcion,
                        "url"=>$url,
                        "redirect"=>$redirect,
                        "file_height"=>$file_height,
                    ));
                }else{
                    echo "No se encontraron datos para este reporte";
                }
                
            }
        }
    }

    public function reporte_impuestos(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] > 3){
            if(isset($_GET["data"]) && isset($_GET["s"])){
            #### CONFIGURACION ####
            $start_date = $_GET["data"];
            $end_date = $_GET["s"];
            #### CONFIGURACION ####
            //llamando la libreria pdf
            //require_once 'lib/printpdf/fpdf.php';
            //Models
            $venta = new Ventas($this->adapter);
            $compra = new Compras($this->adapter);

            $ventacontable = new VentaContable($this->adapter);
            $detalleventa = new DetalleVenta($this->adapter);
            $detalleventacontable = new DetalleVentaContable($this->adapter);

            //Functions
            $state = false;
            $ventas = $venta->reporte_general($start_date,$end_date);
            $compras = $compra->reporte_general($start_date,$end_date);
            foreach($ventas as $dataventas){}
            if($dataventas){$state=true;}

            /////recuperando la sucursal por factura de venta
            $sucursales = new Sucursal($this->adapter);
            $global = new sGlobal($this->adapter);
            $sucursal = $sucursales->getSucursalById($_SESSION["idsucursal"]);
            $empresa = $global->getGlobal();
            foreach ($sucursal as $sucursal) {}
            foreach ($empresa as $empresa) {}

            if($state){
                //Modelado de factura pdf
                $resp=[];
                $resp2=[];
                $resp3=[];
                $subtotal=0;
                $subtotal_importe=0;
                $total =0;
                $subtotal_compra=0;
                $subtotal_importe_compra=0;
                $total_compra =0;
                //load data
                foreach($ventas as $data){
                    if($data->estado == "A"){
                    $resp2[] = array(
                        $data->serie_comprobante,
                        zero_fill($data->num_comprobante,8),
                        $data->fecha,
                        $data->sub_total,
                        "19",
                        $data->subtotal_importe,
                        $data->total,
                    );
                }
                    $subtotal += $data->sub_total;
                    $subtotal_importe += $data->subtotal_importe;
                    $total += $data->total;
                }
                $resp2[] = array(
                    "",
                    "TOTAL IVA EN VENTAS",
                    "",
                    $subtotal,
                    "",
                    $subtotal_importe,
                    $total,
                );

                foreach($compras as $data_compra){
                    if($data_compra->estado == "A"){
                    $resp3[]=array(
                        $data_compra->serie_comprobante,
                        zero_fill($data_compra->num_comprobante,8),
                        $data_compra->fecha,
                        $data_compra->sub_total,
                        "19",
                        $data_compra->subtotal_importe,
                        $data_compra->total
                    );
                }
                    $subtotal_compra += $data_compra->sub_total;
                    $subtotal_importe_compra += $data_compra->subtotal_importe;
                    $total_compra += $data_compra->total;
                }

                $resp3[] = array(
                    "",
                    "TOTAL IVA EN COMPRAS",
                    "",
                    $subtotal_compra,
                    "",
                    $subtotal_importe_compra,
                    $total_compra,
                );




                $pdf = new FPDF();
                $pdf->SetTitle("Reporte de impuestos ".$start_date." hasta ".$end_date);
                $pdf->AddPage('L','A4');

                //>>>>>>>>>header 
                $x =10; 
                $y =0;
                $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,30,12,36);
                $pdf->SetFont('Arial','B',8);
                $pdf->Cell(0,6,$sucursal->razon_social,0,1,'C');
                $pdf->SetFont('Arial','',8.5);
                $pdf->Cell(0,0,$sucursal->prefijo_documento." ".$sucursal->num_documento,0,1,'C');
                $pdf->Cell(0,6,$sucursal->direccion,0,1,'C');
                $pdf->Cell(0,0,"Tel: ".$sucursal->telefono,0,1,'C');
                $pdf->Cell(0,5,$empresa->pais." - ".$empresa->ciudad,0,1,'C');
                $pdf->Cell(0,1,$sucursal->email,0,1,'C');

                $pdf->SetFont('Arial','B',10.5);
                // $pdf->Cell(0,10,"IVA GENERADO EN VENTAS",0,1,'C');
                // $pdf->Cell(0,6,"IVA DESCONTABLE EN COMPRAS",0,1,'C');
                
                $pdf->SetFont('Arial','',8.5);
                $infofecha = array("Fecha desde","Fecha hasta");
                $date = array($start_date,$end_date);

                //<<<<<<<<<header
                //>>>>>>>body
                $pdf->SetY(20);
                $pdf->SetX(210);
                $pdf->DateTable($infofecha,$resp);
                $pdf->SetY(27);
                $pdf->SetX(210);
                $pdf->DateTable($date,$resp);


                $pdf->SetY(40);
                $pdf->SetX(10);
                $tablehead = array("TIPO DOC","CONSECUTIVO","FECHA PROCESO","VALOR SUBTOTAL","% IVA","VALOR IVA", "NETO");
                $pdf->FancyTableImpuesto($tablehead,$resp2,"IVA GENERADO EN VENTAS");

                $pdf->AddPage('L','A4');

                //>>>>>>>>>header 
                $x =10; 
                $y =0;
                $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,30,12,36);
                $pdf->SetFont('Arial','B',8);
                $pdf->Cell(0,6,$sucursal->razon_social,0,1,'C');
                $pdf->SetFont('Arial','',8.5);
                $pdf->Cell(0,0,$sucursal->prefijo_documento." ".$sucursal->num_documento,0,1,'C');
                $pdf->Cell(0,6,$sucursal->direccion,0,1,'C');
                $pdf->Cell(0,0,"Tel: ".$sucursal->telefono,0,1,'C');
                $pdf->Cell(0,5,$empresa->pais." - ".$empresa->ciudad,0,1,'C');
                $pdf->Cell(0,1,$sucursal->email,0,1,'C');

                
                
                $pdf->FancyTableImpuesto($tablehead,$resp3,"IVA DESCONTABLE EN COMPRAS");
                

                //<<<<<<<body

                $pdf->AutoPrint();
                $pdf->Output("Reporte de impuestos ".$start_date." hasta ".$end_date.".pdf","I");
                
            }

        }else{
            echo "Forbidden gateway";
        }
        }else{
            echo "Forbidden gateway";
        }
    }

    public function factura_venta()
    {

        if(isset($_GET["data"]) && !empty($_GET["data"])){
            //require_once 'lib/printpdf/fpdf.php';
                $cifrasEnLetras = new CifrasEnLetras();
                $pieFactura = new PieFactura($this->adapter);
                $detalleretencion = new DetalleRetencion($this->adapter);
                $detalleimpuesto = new DetalleImpuesto($this->adapter);
                $dataimpuestos = new Impuestos($this->adapter);
                $dataretencion = new Retenciones($this->adapter);
                $idventa = $_GET["data"];
                //recuperando la venta por id
                $ventas = new Ventas($this->adapter);
                $venta = $ventas->getVentaById($idventa);
                if($venta){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($venta as $data){}
                $resolucion = $pieFactura->getPieFacturaByComprobanteId($data->iddetalle_documento_sucursal);
                foreach ($resolucion as $res) {}
                $dataarticulos = new DetalleVenta($this->adapter);
                $articulos = $dataarticulos->getArticulosByVenta($idventa);
                //recuperando la sucursal por factura de venta
                $sucursales = new Sucursal($this->adapter);
                $global = new sGlobal($this->adapter);
                $sucursal = $sucursales->getSucursalById($data->idsucursal);
                $empresa = $global->getGlobal();
                foreach ($sucursal as $sucursal) {}
                foreach ($empresa as $empresa) {}
                $resp=[];
                $resp2=[];
                $pdf = new FPDF('P','mm',array(100,150),$this->adapter);
                $pdf->SetTitle("Factura de venta ".$data->serie_comprobante.zero_fill($data->num_comprobante,8)." ".$data->fecha." Cliente ".$data->nombre_cliente);

                $x =10; 
                $y =0;

                $array =array(
                    "tercero"=>$data->nombre_cliente,
                    "documento"=>$data->num_documento,
                    "telefono"=>$data->telefono_cliente,
                    "direccion"=>$data->direccion_calle,
                    "ciudad"=>$data->direccion_provincia,
                    "start_date"=>$data->fecha,
                    "end_date"=>$data->fecha_final,
                    "tipo_doc"=>"FACT. VENTA No.",
                    "comprobante"=>$data->serie_comprobante.zero_fill($data->num_comprobante,8),
                );
            
                $pdf->setData($array);
                $pdf->AddPage('P','A4','');

                //BODY
                //TABLE HEAD
                foreach ($articulos as $articulo) {
                    $resp2[] = array(
                        $articulo->idarticulo,
                        $articulo->descripcion,
                        $articulo->cantidad,
                        $articulo->precio_unitario,
                        $articulo->iva_compra,
                        $articulo->precio_total_lote
                    );
                }
                $tablehead = array("Codigo","Producto","Cantidad","Precio U.","IVA","Total");
                $pdf->SetY(110);
                $pdf->SetX(10);
                $pdf->FancyTable($tablehead,$resp2);
                //TABLE BODY
                $retenciones = $detalleretencion->getRetencionBy($idventa,0);

                $impuestos = $detalleimpuesto->getImpuestosBy($idventa,0);

                //$totalcart = new DetalleIngreso($this->adapter);
                $subtotal = $dataarticulos->getSubTotal($data->idventa);
                $totalimpuestos = $dataarticulos->getImpuestos($data->idventa);

             //obter subtotal
            foreach ($subtotal as $subtotal) {}
            //valores a imprimir
            $subtotalimpuesto = 0;
            $listImpuesto = [];
            $listRetencion =[];
            $total_bruto = $subtotal->cdi_debito;
            $total_neto = $subtotal->cdi_debito;
            //obtener impuestos en grupos por porcentaje (19% 10% 5% etc...)
            foreach ($totalimpuestos as $imp) {
                $subtotalimpuesto += $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                foreach($impuestos as $data2){}
                if($impuestos){
                   
                    $total_neto -= $subtotalimpuesto;
                    $total_bruto -= $subtotalimpuesto;
                
                }else{
                    
                }
                
                foreach ($impuestos as $impuesto) {
                    if($imp->cdi_importe == $impuesto->im_porcentaje){
                        //calculado
                        $calc = $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                        //concatenacion del nombre
                        $im_nombre = $impuesto->im_nombre." ".$impuesto->im_porcentaje."%";
                        //arreglo
                        $listImpuesto[] = array($im_nombre,$calc);
                        /************************SUMANDO IMPUESTOS DEL CALCULO*****************************/
                        $total_neto += $calc;
                    }else{
                       
                    }
                }
            }
                foreach ($retenciones as $retencion) {
                
                    if($retencion->re_im_id <= 0){
                        //concatenacion del nombre
                        $re_nombre = $retencion->re_nombre." ".$retencion->re_porcentaje."%";
                        //calculado $subtotal->cdi_debito*($retencion->re_porcentaje/100)
                        $calc = $total_bruto* ($retencion->re_porcentaje/100);
                        //arreglo
                        $listRetencion[] = array($re_nombre,$calc);
                        /************************RESTANDO RETENCION DEL CALCULO*****************************/
                        $total_neto -= $calc;
                    }else{
                    foreach ($totalimpuestos as $imp) {
                    $impid = $dataimpuestos->getImpuestosById($retencion->re_im_id);
                    foreach ($impid as $impid) {
                        if($imp->cdi_importe == $impid->im_porcentaje){
                            $re_nombre = $retencion->re_nombre." (".$retencion->re_porcentaje."%)";
                            $iva =$imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));

                            $calc =$iva*($retencion->re_porcentaje/100);

                            $listRetencion[] = array($re_nombre,$calc);
                            /************************RESTANDO RETENCION DEL CALCULO*****************************/
                            $total_neto -= $calc;
                        }else{
                        }
                    }                    
                }
            }
                
            }
            //variables
            $totalenletras =$cifrasEnLetras->convertirNumeroEnLetras($total_neto);
            $text_resolucion = explode('|', $res->pf_text);
            $prices = [];
            $ry=154;
            $i=0;
            $pdf->setValorEnLetras("Valor en letras: ".$totalenletras." Pesos colombianos");
            foreach ($text_resolucion as $content) {
                $i++;
            }
            $pdf->setResolucion($res->pf_text);

            $prices[] = array("SUBTOTAL:"=>"$".number_format($total_bruto,2,'.',','));
            foreach($listImpuesto as $listImpuesto){
                $prices[]=array($listImpuesto[0].":"=>"$".number_format($listImpuesto[1],2,'.',',')); 
            }
            foreach ($listRetencion as $listRetencion) {
                $prices[]=array($listRetencion[0]=>"$".number_format($listRetencion[1],2,'.',','));
            }
            $prices[]=array("TOTAL:"=>"$".number_format($total_neto,2,'.',','));
            $pdf->setPrices($prices);
            $pdf->AutoPrint();
        
            $pdf->Output($data->serie_comprobante.zero_fill($data->num_comprobante,8)." ".$data->fecha." Cliente ".$data->nombre_cliente.".pdf","I");

        }else{
            echo "Forbidden Gateway";
        }
    }
    }

    public function ingreso()
    {
        if(isset($_SESSION["idsucursal"]) && $_SESSION["permission"] > 1){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                
                $idcompra = $_GET["data"];
                //configuracion para el modo de visualizacion
                $view = (isset($_GET["s"])&&!empty($_GET["s"]))?$_GET["s"]:"";
                $file_height = (isset($view))?"100%":"92.4%";
                $conf_print = (isset($_GET["t"]) && !empty($_GET["t"]))?$_GET["t"]:"";
            
                //recuperando la venta por id
                $compras = new Compras($this->adapter);
                $compra = $compras->getCompraById($idcompra);
                if($compra){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($compra as $data){}
                //recuperando la sucursal por factura de venta
                $sucursales = new Sucursal($this->adapter);
                $sucursal = $sucursales->getSucursalById($data->idsucursal);
               
                //traer la vista del tipo de impresion que se aplica a este comprobante
                $funcion = $data->pri_conf."_compra";
                //configuracion solo para desarrollo
                //$location = "http://127.0.0.1/app";
                $location = LOCATION_CLIENT;

              $this->frameview("file/pdf/".$data->pri_nombre,array(
                "file_height"=>$file_height,
                "conf_print"=>$conf_print,
                "venta"=>$compra,
                "sucursal"=>$sucursal,
                "funcion"=>$funcion,
                "id"=>$idcompra,
                "url"=>$location
               ));
                }else{
                    echo "Factura no disponible";
                }
            }else{
                
            }
        }
    }

    public function factura_compra()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >1){
        if(isset($_GET["data"]) && !empty($_GET["data"])){
            //require_once 'lib/printpdf/fpdf.php';
            $idcompra = $_GET["data"];
                ######## clases
                //recuperando la venta por id
                $cifrasEnLetras = new CifrasEnLetras();
                $compras = new Compras($this->adapter);
                $dataretenciones = new Retenciones($this->adapter);
                $dataimpuestos= new Impuestos($this->adapter);
                $sucursales = new Sucursal($this->adapter);
                $global = new sGlobal($this->adapter);
                $dataarticulos = new DetalleIngreso($this->adapter);
                $detalleimpuesto = new DetalleImpuesto($this->adapter);
                $detalleretencion = new DetalleRetencion($this->adapter);
                $pdf = new FPDF('P','mm',array(100,150),$this->adapter);
                $pieFactura = new PieFactura($this->adapter);
            
                $compra = $compras->getCompraById($idcompra);
                if($compra){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($compra as $data){}
                
                $articulos = $dataarticulos->getArticulosByCompra($idcompra);
                //ecuperando la sucursal por factura de venta
                $resolucion = $pieFactura->getPieFacturaByComprobanteId($data->iddetalle_documento_sucursal);
                foreach ($resolucion as $res) {}
                
                $sucursal = $sucursales->getSucursalById($data->idsucursal);
                
                $empresa = $global->getGlobal();
                foreach ($sucursal as $sucursal) {}
                foreach ($empresa as $empresa) {}
                $resp=[];
                $resp2=[];
                foreach ($articulos as $articulo) {
                    $resp2[] = array(
                        $articulo->idarticulo,
                        $articulo->nombre_articulo,
                        $articulo->stock_ingreso,
                        $articulo->precio_compra,
                        $articulo->iva_compra,
                        $articulo->precio_total_lote
                    );
                }

                $pdf->SetTitle("Factura de compra ".$data->serie_comprobante.zero_fill($data->num_comprobante,8)." ".$data->fecha." Proveedor ".$data->nombre_proveedor);
                
                //header
                $x =10;
                $y =0;

                $array =array(
                    "tercero"=>$data->nombre_proveedor,
                    "documento"=>$data->num_documento,
                    "telefono"=>$data->telefono_proveedor,
                    "direccion"=>$data->direccion_calle,
                    "ciudad"=>$data->direccion_provincia,
                    "start_date"=>$data->fecha,
                    "end_date"=>$data->fecha_final,
                    "tipo_doc"=>"REG. COMPRA No.",
                    "comprobante"=>$data->serie_comprobante.zero_fill($data->num_comprobante,8),
                );
            
                $pdf->setData($array);
                $pdf->AddPage('P','A4','');
                
                $tablehead = array("Codigo","Producto","Cantidad","Precio U.","IVA","Total");
                $pdf->SetY(110);
                $pdf->SetX(10);
                $pdf->FancyTable($tablehead,$resp2);

            
                $retenciones = $detalleretencion->getRetencionBy($idcompra,0);
                $impuestos = $detalleimpuesto->getImpuestosBy($idcompra,0);

            //$totalcart = new DetalleIngreso($this->adapter);
            $subtotal = $dataarticulos->getSubTotal($data->idingreso);
            $totalimpuestos = $dataarticulos->getImpuestos($data->idingreso);
            
            //obter subtotal
            foreach ($subtotal as $subtotal) {}
            //valores a imprimir
            $subtotalimpuesto = 0;
            $listImpuesto = [];
            $listRetencion =[];
            $total_bruto = $subtotal->cdi_debito;
            $total_neto = $subtotal->cdi_debito;
            //obtener impuestos en grupos por porcentaje (19% 10% 5% etc...)
            foreach ($totalimpuestos as $imp) {
                $subtotalimpuesto += $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                foreach($impuestos as $data2){}
                if($impuestos){
                   if($data2->im_porcentaje == $imp->cdi_importe){
                    $total_neto -= $subtotalimpuesto;
                    $total_bruto -= $subtotalimpuesto;
                   }
                   else{

                   }
                }else{
                
                }
                
                foreach ($impuestos as $impuesto) {
                    if($imp->cdi_importe == $impuesto->im_porcentaje){
                        //calculado
                        $calc = $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                        //concatenacion del nombre
                        $im_nombre = $impuesto->im_nombre." ".$impuesto->im_porcentaje."%";
                        //arreglo
                        $listImpuesto[] = array($im_nombre,$calc);
                        /************************SUMANDO IMPUESTOS DEL CALCULO*****************************/
                        $total_neto += $calc;
                    }else{
                       
                    }
                }
            }
                foreach ($retenciones as $retencion) {
                
                    if($retencion->re_im_id <= 0){
                        //concatenacion del nombre
                        $re_nombre = $retencion->re_nombre." ".$retencion->re_porcentaje."%";
                        //calculado $subtotal->cdi_debito*($retencion->re_porcentaje/100)
                        $calc = $total_bruto* ($retencion->re_porcentaje/100);
                        //arreglo
                        $listRetencion[] = array($re_nombre,$calc);
                        /************************RESTANDO RETENCION DEL CALCULO*****************************/
                        $total_neto -= $calc;
                    }else{
                    foreach ($totalimpuestos as $imp) {
                    $impid = $dataimpuestos->getImpuestosById($retencion->re_im_id);
                    foreach ($impid as $impid) {
                        if($imp->cdi_importe == $impid->im_porcentaje){
                            $re_nombre = $retencion->re_nombre." (".$retencion->re_porcentaje."%)";
                            $iva =$imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));

                            $calc =$iva*($retencion->re_porcentaje/100);

                            $listRetencion[] = array($re_nombre,$calc);
                            /************************RESTANDO RETENCION DEL CALCULO*****************************/
                            $total_neto -= $calc;
                        }else{
                        }
                    }                    
                }
            }
                
            }
            $prices = [];
            $y=160;
            $x=205;
            $totalenletras =$cifrasEnLetras->convertirNumeroEnLetras($total_neto);
            
            $prices[] = array("SUBTOTAL:"=>"$".number_format($total_bruto,2,'.',','));
            foreach($listImpuesto as $listImpuesto){
            $prices[]= array($listImpuesto[0].":"=>"$".number_format($listImpuesto[1],2,'.',',')); 
            }
            foreach ($listRetencion as $listRetencion) {
                $prices[]=array($listRetencion[0]=>"$".number_format($listRetencion[1],2,'.',',')); 
            }
            $prices[]=array("TOTAL:"=>"$".number_format($total_neto,2,'.',','));

            $pdf->setValorEnLetras("Valor en letras: ".$totalenletras." Pesos colombianos");
            $pdf->setResolucion($res->pf_text);
            $pdf->setPrices($prices);
            $pdf->AutoPrint();
            $pdf->Output($data->serie_comprobante.zero_fill($data->num_comprobante,8)." ".$data->fecha." Cliente ".$data->nombre_proveedor.".pdf","I");

        }else{
            echo "Forbidden Gateway";
        }
    }
    }else{
        echo "Forbidden Gateway";
    }

    }


    public function pos_venta()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >1){
        if(isset($_GET["data"]) && !empty($_GET["data"])){
        
        //require_once 'lib/printpdf/fpdf.php';
        $idventa = $_GET["data"];
        ############## modelos
        $pdf = new FPDF($orientation='P',$unit='mm', array(45,350));
        $sucursales = new Sucursal($this->adapter);
        $ventas = new Ventas($this->adapter);
        $detalleventa = new DetalleVenta($this->adapter);
        $dataretenciones = new Retenciones($this->adapter);
        $dataimpuestos= new Impuestos($this->adapter);
        $pieFactura = new PieFactura($this->adapter);
        $cifrasEnLetras = new CifrasEnLetras();
        ############## funciones
        $venta = $ventas->getVentaById($idventa);
        foreach($venta as $data){}
        $resolucion = $pieFactura->getPieFacturaByComprobanteId($data->iddetalle_documento_sucursal);
        foreach ($resolucion as $resolucion) {}
        $sucursal = $sucursales->getSucursalById($data->idsucursal); 
        $detalle = $detalleventa->getArticulosByVenta($idventa);

        $retenciones = $dataretenciones->getRetencionesByComprobanteId($data->iddetalle_documento_sucursal);
        $impuestos = $dataimpuestos->getImpuestosByComprobanteId($data->iddetalle_documento_sucursal);
        //detalle venta
        $subtotal = $detalleventa->getSubTotal($data->idventa);
        $totalimpuestos = $detalleventa->getImpuestos($data->idventa);

        ######## obtener solo una vista de la funcion llamada
        foreach ($sucursal as $sucursal) {}
        foreach ($venta as $venta) {}
        
        
        $pdf->AddPage();
        $x =10;
        $y =0;
        $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,12,2,22);
        $pdf->SetFont('Arial','B',5);    //Letra Arial, negrita (Bold), tam. 20
        $textypos = 10;
        $pdf->setY(2);
        $pdf->setX(2);
        $pdf->SetFont('Arial','B',4.5);
        
        $pos = 0;
        $pdf->SetXY(0, 18);
        $pdf->SetX(10);
        $pdf->Cell(0,$pos,utf8_decode($sucursal->razon_social),0,0,'C');

        $pdf->SetFont('Arial','',4); 

        $pdf->SetX(11.5);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,utf8_decode($sucursal->telefono." - ".$sucursal->prefijo_documento." ".$sucursal->num_documento),0,0,'C');

        $pdf->SetX(11.5);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,utf8_decode($sucursal->ciudad." - ".$sucursal->pais),0,0,'C');

        $pdf->SetFont('Arial','B',4);
        $pdf->SetX(11.5);
        $pos = $pos + 5;
        $pdf->Cell(0,$pos,"Ticket de venta  ".$venta->serie_comprobante.zero_fill($venta->num_comprobante,8),0,0,'C');

        $pdf->SetFont('Arial','',4); 
        $pdf->SetX(1);
        $pos = $pos + 5;
        $pdf->Cell(0,$pos,"Fecha:  ".utf8_decode($venta->fecha)."                           Fecha final: ".utf8_decode($venta->fecha_final),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Vendedor:  ".utf8_decode($venta->idusuario),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Tipo de Venta:  ".utf8_decode($venta->tipo_pago),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Cliente:  ".utf8_decode($venta->nombre_cliente),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Documento:  ".utf8_decode($venta->num_documento),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Direccion:  ".utf8_decode($venta->direccion_calle),0,0,'L');

        $pdf->SetX(1);
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,"Telefono:  ".utf8_decode($venta->telefono),0,0,'L');

        $pdf->SetFont('Arial','',3);    //Letra Arial, negrita (Bold), tam. 20
        $pos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,'-----------------------------------------------------------------------------------------------------------------');
        $pos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,'CANT.           DESCRIPCION                                IMPORTE                            TOTAL');
        
        $total =0;
        $off = $pos+6;

        $i=0;
        foreach($detalle as $detalle){
        $pdf->setX(2);
        $pdf->Cell(5,$off,$detalle->cantidad.utf8_decode($detalle->prefijo_medida));
        $pdf->setX(8);
        $pdf->Cell(35,$off,  strtoupper(substr(utf8_decode($detalle->nombre_articulo), 0,25)) );
        $pdf->setX(20);
        $pdf->Cell(12,$off,  "$".number_format($detalle->iva_compra,2,".",",") ,0,0,"R");
        $pdf->setX(32);
        $pdf->Cell(11,$off,  "$ ".number_format($detalle->precio_total_lote,2,".",",") ,0,0,"R");
        
        $total += $detalle->precio_total_lote;
        $off+=6;
        $i++;
        }
        $pos=$off+6;

        // $pdf->setX(2);
        // $pdf->Cell(5,$pos,"TOTAL: " );
        // $pdf->setX(38);
        // $pdf->Cell(5,$pos,"$ ".number_format($total,2,".",","),0,0,"R");
        //$pos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,utf8_decode("Nº de artículos: ".$i));
        $pdf->setX(38);
        //obter subtotal
        foreach ($subtotal as $subtotal) {}
        //valores a imprimir
        $subtotalimpuesto = 0;
        $listImpuesto = [];
        $listRetencion =[];
        $total_bruto = $subtotal->cdi_debito;
        $total_neto = $subtotal->cdi_debito;
        //obtener impuestos en grupos por porcentaje (19% 10% 5% etc...)
        foreach ($totalimpuestos as $imp) {
            $subtotalimpuesto += $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
            foreach($impuestos as $data){}
            if($impuestos){
               
                $total_neto -= $subtotalimpuesto;
                $total_bruto -= $subtotalimpuesto;
            
            }else{
                
            }
            
            foreach ($impuestos as $impuesto) {
                if($imp->cdi_importe == $impuesto->im_porcentaje){
                    //calculado
                    $calc = $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                    //concatenacion del nombre
                    $im_nombre = $impuesto->im_nombre." ".$impuesto->im_porcentaje."%";
                    //arreglo
                    $listImpuesto[] = array($im_nombre,$calc);
                    /************************SUMANDO IMPUESTOS DEL CALCULO*****************************/
                    $total_neto += $calc;
                }else{
                   
                }
            }
        }
            foreach ($retenciones as $retencion) {
            
                if($retencion->re_im_id <= 0){
                    //concatenacion del nombre
                    $re_nombre = $retencion->re_nombre." ".$retencion->re_porcentaje."%";
                    //calculado $subtotal->cdi_debito*($retencion->re_porcentaje/100)
                    $calc = $total_bruto* ($retencion->re_porcentaje/100);
                    //arreglo
                    $listRetencion[] = array($re_nombre,$calc);
                    /************************RESTANDO RETENCION DEL CALCULO*****************************/
                    $total_neto -= $calc;
                }else{
                foreach ($totalimpuestos as $imp) {
                $impid = $dataimpuestos->getImpuestosById($retencion->re_im_id);
                foreach ($impid as $impid) {
                    if($imp->cdi_importe == $impid->im_porcentaje){
                        $re_nombre = $retencion->re_nombre." (".$retencion->re_porcentaje."%)";
                        $iva =$imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));

                        $calc =$iva*($retencion->re_porcentaje/100);

                        $listRetencion[] = array($re_nombre,$calc);
                        /************************RESTANDO RETENCION DEL CALCULO*****************************/
                        $total_neto -= $calc;
                    }else{
                    }
                }                    
            }
        }
            
        }

        $pos+=5;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,"Subtotal: " );
        $pdf->setX(38);
        $pdf->Cell(5,$pos,"$ ".number_format($total_bruto,2,".",","),0,0,"R");
        foreach($listImpuesto as $listImpuesto){
            $pos+=5;
            $pdf->setX(2);
            $pdf->Cell(5,$pos,$listImpuesto[0]);
            $pdf->setX(38);
            $pdf->Cell(5,$pos,"$ ".number_format($listImpuesto[1],2,".",","),0,0,"R");
        }
        foreach ($listRetencion as $listRetencion) {
            $pos+=5;
            $pdf->setX(2);
            $pdf->Cell(5,$pos,$listRetencion[0]);
            $pdf->setX(38);
            $pdf->Cell(5,$pos,"$ ".number_format($listRetencion[1],2,".",","),0,0,"R");
        }
        $pos+=5;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,"Total: " );
        $pdf->setX(38);
        $pdf->Cell(5,$pos,"$ ".number_format($total_neto,2,".",","),0,0,"R");

        $text_resolucion = explode('|', $resolucion->pf_text);
            $pdf->SetFont('Arial','',3); 
            $pdf->SetX(0);
            $pos = $pos + 12;
            
            $ry=$pos+=5;
            $i=0;
            foreach ($text_resolucion as $content) {
                $pdf->SetX(15);
                $pdf->SetY(15);
                $pos = $pos + 3;
                $pdf->Cell(0,$pos,utf8_decode($text_resolucion[$i]),0,0,'C');
                $i++;
            }
            $pdf->SetX(15);
            $pdf->SetY(17);
            $pos = $pos + 3;
            $pdf->Cell(0,$pos,utf8_decode("*GRACIAS POR SU COMPRA*"),0,0,'C');
            $pdf->SetX(15);
            $pdf->SetY(17);
            $pos = $pos + 3;
            $pdf->Cell(0,$pos,utf8_decode($sucursal->razon_social),0,0,'C');

        $pdf->AutoPrint();
        $pdf->output();

    }else{

    }
    }else{
        echo "Forbidden gateway";
    }
    } 

    public function cierre_caja()
    {
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                //require_once 'lib/printpdf/fpdf.php';

                $idreporte = $_GET["data"];
                //recuperando la venta por id
                $caja = new ReporteCaja($this->adapter);
                $reporte = $caja->getReporteById($idreporte);
                if($reporte){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($reporte as $data){}
                //ecuperando la sucursal por factura de venta
                $sucursales = new Sucursal($this->adapter);
                $venta = new Ventas($this->adapter);

                $sucursal = $sucursales->getSucursalById($data->rc_idsucursal);
                foreach ($sucursal as $sucursal) {}
                $ventas = $venta->reporte_detallado_categoria($data->rc_fecha,$data->rc_fecha);
                $articulos = $venta->reporte_detallado_articulo($data->rc_fecha,$data->rc_fecha);
                $fecha_reporte =$data->rc_fecha;


        
        $pdf = new FPDF($orientation='P',$unit='mm', array(45,350));
        $pdf->SetTitle("Cierre de caja ".$data->rc_fecha);
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',4.5);
        
        $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,12,2,22);
        $textypos = 5;
        $pos = 24;
        $pdf->SetXY(0, 5);
        $pdf->SetX(10);
        $pdf->Cell(0,$pos,$sucursal->razon_social,0,0,'C');
        $pdf->SetX(11.5);

        $pdf->SetFont('Arial','',4); 
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,$sucursal->ciudad." - ".$sucursal->pais,0,0,'C');

        $pdf->SetX(11.5);
        $pdf->SetFont('Arial','',3.5); 
        $pos = $pos + 3;
        $pdf->Cell(0,$pos,$sucursal->prefijo_documento." ".$sucursal->num_documento,0,0,'C');


        $pdf->SetX(11.5);
        $pdf->SetFont('Arial','B',3.7); 
        $pos = $pos + 5;
        $pdf->Cell(0,$pos,"CIERRE DE CAJA ".$data->rc_fecha,0,0,'C');

        $pdf->SetFont('Arial','B',3.5); 
        $pos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,'----------------------------------------Categoria---------------------------------------------');
        $pos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$pos,'CATEGORIA                   SUBTOTAL                IVA                 NETO');
        
        $total =0;
        $off = $pos+6;
        foreach($ventas as $ventas){
        $pdf->setX(2);
        $pdf->Cell(5,$off,$ventas->nombre_categoria);
        $pdf->setX(16);
        $pdf->Cell(35,$off,  number_format($ventas->precio_categoria,2,',','.') );
        $pdf->setX(22);
        $pdf->Cell(11,$off,  "$".number_format(($ventas->precio_importe_categoria*$ventas->cantidad),2,',','.') ,0,0,"R");
        $pdf->setX(32);
        $pdf->Cell(11,$off,  "$ ".number_format($ventas->precio_categoria+$ventas->precio_importe_categoria,2,',','.') ,0,0,"R");
        
        $total += $ventas->precio_categoria+$ventas->precio_importe_categoria;
        $off+=6;
        }
        $pdf->SetFont('Arial','B',3.5); 
        $off+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$off,'------------------------------------------Articulo-------------------------------------------');
        $off+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$off,'ARTICULO                   SUBTOTAL                IVA                 NETO');
        $off+=6;
        foreach($articulos as $articulos){
            $pdf->setX(2);
            $pdf->Cell(5,$off,substr($articulos->nombre_articulo,0,16));
            $pdf->setX(16);
            $pdf->Cell(35,$off,  number_format($articulos->precio_categoria,2,',','.') );
            $pdf->setX(22);
            $pdf->Cell(11,$off,  "$".number_format($articulos->precio_importe_categoria,2,',','.') ,0,0,"R");
            $pdf->setX(32);
            $pdf->Cell(11,$off,  "$ ".number_format($articulos->precio_categoria+$articulos->precio_importe_categoria,2,',','.') ,0,0,"R");
        
            $off+=6;
            }
        $textypos=$off+6;
        
        $pdf->setX(2);
        $pdf->Cell(5,$textypos,"TOTAL VENTAS: " );
        $pdf->setX(38);
        $pdf->Cell(5,$textypos,"$ ".number_format($total,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'EFECTIVO:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ ".number_format($data->rc_efectivo,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'CREDITO:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ ".number_format($data->rc_credito,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'DEBITO:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ ".number_format($data->rc_debito,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'PAGOS:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ -".number_format($data->rc_pagos,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'TOTAL CAJA:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ ".number_format($data->rc_monto,2,",","."),0,0,"R");

        $textypos+=6;
        $pdf->setX(2);
        $pdf->Cell(5,$textypos+6,'DIFERENCIA:');
        $pdf->setX(38);
        $pdf->Cell(5,$textypos+6,"$ ".number_format(($data->rc_monto+$data->rc_pagos)-$total,2,",","."),0,0,"R");

        $pdf->AutoPrint();
        $pdf->output("Cierre de caja ".$fecha_reporte.".pdf","I");
        }

        

        }
    }

    public function cartera()
    {
        if(isset($_GET["data"]) && !empty($_GET["data"]) && isset($_GET["s"]) && !empty($_GET["s"])){

        //Configuracion solo para desarrollo 
        //$location = "http://127.0.0.1/app";
        $location = LOCATION_CLIENT;
        $funcion = "pago_cartera";
        $data = $_GET["data"];
        $s = $_GET["s"];

        $this->frameview("file/pdf/cartera",array(
            "url"=>$location,
            "funcion"=>$funcion,
            "data"=>$data,
            "s"=>$s
        ));

        }else{

        }
    }

    public function pago_cartera()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >3){
            if(isset($_GET["data"]) && !empty($_GET["data"]) && isset($_GET["s"]) && !empty($_GET["s"])){
                //require_once 'lib/printpdf/fpdf.php';
                $idcredito = $_GET["s"];
                $cartera = new Cartera($this->adapter);
                $sucursales = new Sucursal($this->adapter);
                $compras = new Compras($this->adapter);
                $ventas = new Ventas($this->adapter);
                $global = new sGlobal($this->adapter);
                $personas = new Persona($this->adapter);
                $comprobantecontable = new ComprobanteContable($this->adapter);
                $detallecomprobantecontable = new DetalleComprobanteContable($this->adapter);
                $resp2=[];
                $empresa = $global->getGlobal();
                foreach ($empresa as $empresa) {}

                if($_GET["data"] == "cliente"){
                    $credito = $cartera->getCreditoClienteById($idcredito);
                    foreach ($credito as $credito) {}
                    $factura = $ventas->getVentaById($credito->idventa);
                    $tercero = $credito->nombre_cliente;
                    $telefono = $credito->telefono;
                    $fecha = $credito->fecha_pago;
                    $deuda_total =$credito->deuda_total;
                    
                }else{
                    $credito = $cartera->getCreditoProveedorById($idcredito);
                    foreach ($credito as $credito) {}
                    $factura = $compras->getCompraById($credito->idingreso);
                    $tercero = $credito->nombre_proveedor;
                    $telefono = $credito->telefono;
                    $fecha = $credito->fecha_pago;
                    $deuda_total =$credito->deuda_total;
                }
                

                $sucursal = $sucursales->getSucursalById($_SESSION["idsucursal"]);

                foreach ($sucursal as $sucursal) {}
                foreach ($factura as $factura) {}
                
                
                if($credito->contabilidad ==1){
                    
                    $detallecomprobante = $detallecomprobantecontable->getArticulosByComprobante($credito->cc_id_transa);
                    $persona = $personas->getPersonaById($credito->cc_idproveedor);
                    foreach($persona as $persona){}
                    foreach($detallecomprobante as $detalle){

                        $datadebito = ($detalle->dcc_d_c_item_det=="D"&& $detalle->dcc_valor_item > 0)?number_format($detalle->dcc_valor_item,2,'.',','):"";
                        $datacredito = ($detalle->dcc_d_c_item_det=="C"&& $detalle->dcc_valor_item > 0)?number_format($detalle->dcc_valor_item,2,'.',','):"";

                        $resp2[]=array(
                            $detalle->dcc_cta_item_det,
                            $detalle->dcc_det_item_det,
                            number_format($detalle->dcc_cant_item_det,2),
                            $credito->cc_ccos_cpte,
                            $persona->num_documento,
                            $detalle->dcc_dato_fact_prove,
                            '',
                            $detalle->dcc_base_ret_item,
                            $datadebito,
                            $datacredito,
                        );

                    }
                    $pdf = new FPDF();
                    $pdf->SetTitle("Pago cartera");
                    $pdf->AddPage("P","A4");

                    $x =10;
                    $y =0;
                    //imprimir cabecera
                    $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,30,12,36);
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(0,6,$sucursal->razon_social,0,1,'C');
                    $pdf->SetFont('Arial','',8.5);
                    $pdf->Cell(0,0,$sucursal->prefijo_documento." ".$sucursal->num_documento,0,1,'C');
                    $pdf->Cell(0,6,$sucursal->direccion,0,1,'C');
                    $pdf->Cell(0,0,"Tel: ".$sucursal->telefono,0,1,'C');
                    $pdf->Cell(0,5,$empresa->pais." - ".$empresa->ciudad,0,1,'C');
                    $pdf->Cell(0,1,$sucursal->email,0,1,'C');
                    $pdf->SetY(12);
                    $pdf->SetX(140);
                    $pdf->Cell(55,16,"REG. COMPRA No.".$credito->cc_num_cpte.zero_fill($credito->cc_cons_cpte,8),1,0,'C');
                    //variables para imprimir sub cabecera de datos de tercero
                    $datatercero = array('Proveedor:', $persona->nombre_persona);
                    $contacto = array('NIT:',$persona->num_documento,"Telefono:",$persona->telefono);
                    $ubicacion = array("Direccion:",$persona->direccion_calle,"Ciudad:",$persona->direccion_provincia);
                    $infofecha = array("Fecha de Factura","Fecha de Vencimiento");
                    $date = array($credito->cc_fecha_cpte,$credito->cc_fecha_final_cpte);
                    $resp=[];
                    //sub cabecera de datos del tercero
                    $pdf->SetFont('Arial','',7);
                    $pdf->SetY(38);
                    $pdf->SetX(10);
                    $pdf->FancyTable($datatercero,$resp);
                    $pdf->SetY(45);
                    $pdf->SetX(10);
                    $pdf->FancyTable($contacto,$resp);
                    $pdf->SetY(52.5);
                    $pdf->SetX(10);
                    $pdf->FancyTable($ubicacion,$resp);
                    // $pdf->SetY(38);
                    // $pdf->SetX(10);
                    // $pdf->DateTable($infofecha,$resp);
                    // $pdf->SetY(38);
                    // $pdf->SetX(140);
                    // $pdf->DateTable($date,$resp);
                    //body

                    $tablehead = array("Cuenta","Detalle","Cant.","C. Costos","Tercero","Doc/Detalle","Fecha Venc.","Base Retencion","Debito","Credito");
                    $pdf->SetY(65);
                    $pdf->SetX(10);
                    $pdf->FancyTableContabilidad($tablehead,$resp2);

                    

                }else{
                $pdf = new FPDF($orientation='P',$unit='mm', array(45,350));
                $pdf->SetTitle("Pago cartera");
                $pdf->AddPage();
                $pdf->SetFont('Arial','B',4.5);
                $pdf->Image(LOCATION_CLIENT.$sucursal->logo_img,12,2,22);
                $textypos = 5;
                $pos = 24;

                $pdf->SetXY(0, 5);
                $pdf->SetX(10);
                $pdf->Cell(0,$pos,$sucursal->razon_social,0,0,'C');
                $pdf->SetX(11.5);

                $pdf->SetFont('Arial','',4); 
                $pos = $pos + 3;
                $pdf->Cell(0,$pos,$sucursal->ciudad." - ".$sucursal->pais,0,0,'C');

                $pdf->SetFont('Arial','',3.5); 

                $pdf->SetX(1);
                $pos = $pos + 4;
                $pdf->Cell(0,$pos,"Comprobante: ".$credito->serie_comprobante.zero_fill($credito->num_comprobante,8),0,0,'L');

                $pdf->SetX(1);
                $pos = $pos + 4;
                $pdf->Cell(0,$pos,"Factura No.: ".$credito->det_fact,0,0,'L');

                $pdf->SetX(1);
                $pos = $pos + 4;
                $pdf->Cell(0,$pos,"Tercero: ".$tercero,0,0,'L');

                $pdf->SetX(1);
                $pos = $pos + 4;
                $pdf->Cell(0,$pos,"Telefono: ".$telefono,0,0,'L');

                $pdf->SetX(1);
                $pos = $pos + 4;
                $pdf->Cell(0,$pos,"Fecha limite: ".$fecha,0,0,'L');

                $pdf->SetFont('Arial','B',3.5); 
                $pos+=6;
                $pdf->setX(2);
                $pdf->Cell(5,$pos,'--------------------------------Pago cartera '.$_GET["data"].'-----------------------------------');
                $pos+=6;
                $pdf->setX(2);
                $pdf->Cell(5,$pos,'FECHA               PAGO PARCIAL           RETENCION           DEVUELTO');
                $pdf->SetFont('Arial','B',2.5); 
                switch ($_GET["data"]) {
                    case 'cliente':
                        $detalle = $cartera->getPagoCarteraCliente($idcredito);

                        break;

                    case 'proveedor':
                        $detalle = $cartera->getPagoCarteraProveedor($idcredito);
                        break;
                    
                    default:
                        $detalle = ["fecha_pago"=>"","pago_parcial"=>"","retencion"=>"","pago"=>""];
                        break;
                }
                        $pagos_realizados =0;
                        $off = $pos+6;
                        foreach ($detalle as $pago) {
                            $pdf->setX(2);
                            $pdf->Cell(5,$off,$pago->fecha_pago);
                            $pdf->setX(14);
                            $pdf->Cell(35,$off,  "$".number_format($pago->pago_parcial,2,',','.') );
                            $pdf->setX(22);
                            $pdf->Cell(11,$off,  "$".number_format($pago->retencion,2,',','.') ,0,0,"R");
                            $pdf->setX(32);
                            $pdf->Cell(11,$off,  "$ ".number_format($pago->monto-$pago->pago_parcial,2,',','.') ,0,0,"R");
                            $pagos_realizados +=$pago->pago_parcial;
                            $off+=6;
                        }

                    $pos=$off+6;
                    $pdf->setX(2);
                    $pdf->Cell(5,$pos,"PAGOS REALIZADOS: " );
                    $pdf->setX(38);
                    $pdf->Cell(5,$pos,"$ ".number_format($pagos_realizados,2,".",","),0,0,"R");

                    $pos+=6;
                    $pdf->setX(2);
                    $pdf->Cell(5,$pos,"DEUDA TOTAL: " );
                    $pdf->setX(38);
                    $pdf->Cell(5,$pos,"$ ".number_format($deuda_total,2,".",","),0,0,"R");

                    $pos+=6;
                    $pdf->setX(2);
                    $pdf->Cell(5,$pos,"DIFERENCIA: " );
                    $pdf->setX(38);
                    $pdf->Cell(5,$pos,"$ ".number_format($deuda_total - $pagos_realizados,2,".",","),0,0,"R");
                }

                $pdf->AutoPrint();
                $pdf->output();

            }else{
                echo "Factura no disponible";
            }
        }else{
            echo "Forbidden Gateway";
        }
    }

    ############################## 

    public function comprobantes(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >1){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                //models
                $comprobantecontable = new ComprobanteContable($this->adapter);
                $sucursales = new Sucursal($this->adapter);
                //functions
                $comprobanteid = $_GET["data"];
                $view = (isset($_GET["s"])&&!empty($_GET["s"]))?$_GET["s"]:"";
                $file_height = (isset($view))?"100%":"92.4%";
                $conf_print = (isset($_GET["t"]) && !empty($_GET["t"]))?$_GET["t"]:$view;
                $return = "#comprobantes/informes/general";

                //recuperando el comprobante contable por id
                $comprobante = $comprobantecontable->getComprobanteById($comprobanteid);
                if($comprobante){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($comprobante as $data){}
                //ecuperando la sucursal por factura de venta
                
                $sucursal = $sucursales->getSucursalById($data->cc_ccos_cpte);

                //traer la vista del tipo de impresion que se aplica a este comprobante
                $funcion = $data->pri_conf."_comprobante";
                //configuracion solo para desarrollo
                //$location = "http://127.0.0.1/";
                $location = LOCATION_CLIENT;

                $this->frameview("file/pdf/".$data->pri_nombre,array(
                    "file_height"=>$file_height,
                    "conf_print"=>$conf_print,
                    "comprobante"=>$comprobante,
                    "sucursal"=>$sucursal,
                    "funcion"=>$funcion,
                    "id"=>$comprobanteid,
                    "url"=>$location
                ));

            }
        }
        }else{
            echo "Forbidden Gateway";
        }
    }

    public function factura_comprobante(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >1){
            if(isset($_GET["data"]) && !empty($_GET["data"]) && $_GET["data"]>0){
                $conf_print = (isset($_GET["s"]) && !empty($_GET["s"]))?$_GET["s"]:false;
                $comprobanteid = $_GET["data"];
                //require_once 'lib/printpdf/fpdf.php';
                //models
                $comprobantecontable = new ComprobanteContable($this->adapter);
                $detallecomprobantecontable = new DetalleComprobanteContable($this->adapter);
                $sucursales = new Sucursal($this->adapter);
                $global = new sGlobal($this->adapter);
                $puc = new PUC($this->adapter);
                $articulo = new Articulo($this->adapter);
                $personas = new Persona($this->adapter);
                $detalleretencion = new DetalleRetencion($this->adapter);
                $detalleimpuesto = new DetalleImpuesto($this->adapter);
                $dataretenciones = new Retenciones($this->adapter);
                $dataimpuestos= new Impuestos($this->adapter);
                $cifrasEnLetras = new CifrasEnLetras();
                $pieFactura = new PieFactura($this->adapter);
                
                ############# funciones
                $comprobante = $comprobantecontable->getComprobanteById($comprobanteid);
                foreach($comprobante as $data){}
                $articulos = $detallecomprobantecontable->getArticulosByComprobante($comprobanteid);

                $totales = $detallecomprobantecontable->getTotalByCompra($comprobanteid);
                $totalimpuestos = $detallecomprobantecontable->getImpuestos($comprobanteid);
                //
                $retenciones = $detalleretencion->getRetencionBy($comprobanteid,1);
                $impuestos = $detalleimpuesto->getImpuestosBy($comprobanteid,1);
                
                //ecuperando la sucursal por factura de venta
                //recuperar sucursal
                $sucursal = $sucursales->getSucursalById($data->cc_ccos_cpte);
                //recuperar empresa 
                $empresa = $global->getGlobal();
                //setear sucursal en variable sucursal
                foreach ($sucursal as $sucursal) {}
                //setear empresa en variable empresa
                foreach ($empresa as $empresa) {}
                //impuestos y retenciones de esta venta


                $resp=[];
                $resp2=[];
                $pdf = new FPDF('P','mm',array(100,150),$this->adapter);
                $pdf->SetTitle("Registro de comprobante ");
                $x =10; 
                $y =0;

                $array =array(
                    "tercero"=>$data->nombre_tercero,
                    "documento"=>$data->documento_proveedor,
                    "telefono"=>$data->telefono_proveedor,
                    "direccion"=>$data->direccion_calle,
                    "ciudad"=>$data->direccion_provincia,
                    "start_date"=>$data->cc_fecha_cpte,
                    "end_date"=>$data->cc_fecha_final_cpte,
                    "tipo_doc"=>$data->prefijo,
                    "comprobante"=>$data->serie_comprobante.zero_fill($data->num_comprobante,8),
                );
                $pdf->setData($array);
                $resolucion = $pieFactura->getPieFacturaByComprobanteId($data->iddetalle_documento_sucursal);
                foreach($resolucion as $res){}
                $pdf->AddPage('P','A4','');
//                $pdf->AddPage('P','A4','',$array);
                $subtotal_credito =0;
                $subtotal_debito =0;
                $subtotal_cuentas =0;
                foreach ($articulos as $articulos) {
                    $getPuc = $puc->getPucById($articulos->dcc_cta_item_det);
                    $gertArticulo = $articulo->getArticuloById($articulos->dcc_cod_art);
                    
                        $debito = ($articulos->dcc_d_c_item_det=="D"&& $articulos->dcc_valor_item > 0)?number_format($articulos->dcc_valor_item,2,'.',','):"";
                        $credito = ($articulos->dcc_d_c_item_det=="C"&& $articulos->dcc_valor_item > 0)?number_format($articulos->dcc_valor_item,2,'.',','):"";
                        $subtotal_credito += ($articulos->dcc_d_c_item_det=="C"&& $articulos->dcc_valor_item > 0)?$articulos->dcc_valor_item:0;
                        $subtotal_debito += ($articulos->dcc_d_c_item_det=="D"&& $articulos->dcc_valor_item > 0)?$articulos->dcc_valor_item:0;
                        
                        $persona = $personas->getPersonaById($data->cc_idproveedor);
                        foreach ($persona as $tercero) {}

                        if($gertArticulo){
                        foreach ($gertArticulo as $articuloitem) {}
                        $fecha_vcto = ($articulos->dcc_fecha_vcto_item !="0000-00-00")?$articulos->dcc_fecha_vcto_item:"";
                        $resp2[] = array(
                            $articulos->dcc_cta_item_det,
                            $articuloitem->descripcion,
                            $articulos->dcc_cant_item_det,
                            $data->cc_ccos_cpte,
                            $tercero->num_documento,
                            $articulos->dcc_dato_fact_prove,
                            $fecha_vcto,
                            $articulos->dcc_base_ret_item,
                            $debito,
                            $credito,
                        );
                        $respStandard[] = array(
                            $articuloitem->idarticulo,
                            $articulos->dcc_det_item_det,
                            number_format($articulos->dcc_cant_item_det,2,',','.'),
                            number_format(($articulos->dcc_valor_item / $articulos->dcc_cant_item_det ),2,',','.'),
                            number_format(($articulos->dcc_valor_item *($articulos->dcc_base_imp_item/100)),2,',','.'),
                            number_format($articulos->dcc_valor_item * (($articulos->dcc_base_imp_item/100)+1),2,',','.')
                        );
                    }elseif($getPuc){
                        foreach ($getPuc as $pucitem) {}
                        $fecha_vcto = ($articulos->dcc_fecha_vcto_item !="0000-00-00")?$articulos->dcc_fecha_vcto_item:"";
                        $resp2[] = array(
                            $pucitem->idcodigo,
                            $articulos->dcc_det_item_det,
                            $articulos->dcc_cant_item_det,
                            $data->cc_ccos_cpte,
                            $tercero->num_documento,
                            $articulos->dcc_dato_fact_prove,
                            $fecha_vcto,
                            $articulos->dcc_base_ret_item,
                            $debito,
                            $credito,
                        );
 
                    }

                    $subtotal_cuentas++; 
                    
                }
                $resp2[] = array(
                    "",
                    $subtotal_cuentas." Cuentas contables",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "Total general:",
                    number_format(round($subtotal_debito)),
                    number_format(round($subtotal_credito)),
                );

            foreach($totales as $subtotal){}
            //valores a imprimir
            $subtotalimpuesto = 0;
            $listImpuesto = [];
            $listRetencion =[];
            $total_bruto = $subtotal->cdi_debito;
            $total_neto = $subtotal->cdi_debito;
            //obtener impuestos en grupos por porcentaje (19% 10% 5% etc...)

            foreach ($totalimpuestos as $imp) {
                $subtotalimpuesto += $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1)); //aqui
                foreach($impuestos as $data2){}
                if($impuestos){
                   if($data2->im_porcentaje == $imp->cdi_importe){
                    $total_neto -= $subtotalimpuesto;
                    $total_bruto -= $subtotalimpuesto;
                   }
                   else{

                   }
                }else{
                
                }
                
                foreach ($impuestos as $impuesto) {
                    if($imp->cdi_importe == $impuesto->im_porcentaje){
                        //calculado
                        $calc = $imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));
                        //concatenacion del nombre
                        $im_nombre = $impuesto->im_nombre." ".$impuesto->im_porcentaje."%";
                        //arreglo
                        $listImpuesto[] = array($im_nombre,$calc);
                        /************************SUMANDO IMPUESTOS DEL CALCULO*****************************/
                        $total_neto += $calc;
                    }else{
                       
                    }
                }
            }
                foreach ($retenciones as $retencion) {
                
                    if($retencion->re_im_id <= 0){
                        //concatenacion del nombre
                        $re_nombre = $retencion->re_nombre." ".$retencion->re_porcentaje."%";
                        //calculado $subtotal->cdi_debito*($retencion->re_porcentaje/100)
                        $calc = $total_bruto* ($retencion->re_porcentaje/100);
                        //arreglo
                        $listRetencion[] = array($re_nombre,$calc);
                        /************************RESTANDO RETENCION DEL CALCULO*****************************/
                        $total_neto -= $calc;
                    }else{
                    foreach ($totalimpuestos as $imp) {
                    $impid = $dataimpuestos->getImpuestosById($retencion->re_im_id);
                    foreach ($impid as $impid) {
                        if($imp->cdi_importe == $impid->im_porcentaje){
                            $re_nombre = $retencion->re_nombre." (".$retencion->re_porcentaje."%)";
                            $iva =$imp->cdi_debito - ($imp->cdi_debito / (($imp->cdi_importe/100)+1));

                            $calc =$iva*($retencion->re_porcentaje/100);

                            $listRetencion[] = array($re_nombre,$calc);
                            /************************RESTANDO RETENCION DEL CALCULO*****************************/
                            $total_neto -= $calc;
                        }else{
                        }
                    }                    
                }
            }
                
            }
            if($conf_print == "standard"){
                //articulos
                $tablehead = array("Codigo","Producto","Cantidad","Precio U.","IVA","Subtotal");
                $pdf->SetY(110);
                $pdf->SetX(10);
                $pdf->FancyTable($tablehead,$respStandard);
                ///cifras en letra
                $totalenletras =$cifrasEnLetras->convertirNumeroEnLetras($total_neto);
                //subtotal
                $prices[] = array("SUBTOTAL:"=>"$".number_format($total_bruto,2,'.',','));
                //impuestos
                foreach($listImpuesto as $listImpuesto){
                $prices[]= array($listImpuesto[0].":"=>"$".number_format($listImpuesto[1],2,'.',',')); 
                }
                //retenciones
                foreach ($listRetencion as $listRetencion) {
                    $prices[]=array($listRetencion[0]=>"$".number_format($listRetencion[1],2,'.',',')); 
                }
                //total
                $prices[]=array("TOTAL:"=>"$".number_format($total_neto,2,'.',','));
                //setear articulos, y precios
                $pdf->setResolucion($res->pf_text);
                $pdf->setValorEnLetras("Valor en letras: ".$totalenletras." Pesos colombianos");
                $pdf->setPrices($prices);

            }else{
                //cuentas contables
                $tablehead = array("Cuenta","Detalle","Cant.","C. Costos","Tercero","Doc/Detalle","Fecha Venc.","Base Retencion","Debito","Credito");
                $pdf->SetY(110);
                $pdf->SetX(10);
                $pdf->FancyTableContabilidad($tablehead,$resp2);
            }

                $pdf->AutoPrint();
                $pdf->output();

            }
        }
    }

    public function informe()
    {
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >0){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                //models
                $sucursales = new Sucursal($this->adapter);
                $reportecontable = new ReporteContable($this->adapter);
                //functions
                $idreporte = $_GET["data"];
                //configuracion para el modo de visualizacion
                $view = (isset($_GET["s"])&&!empty($_GET["s"]))?$_GET["s"]:"";
                $file_height = (isset($view))?"100%":"92.4%";
                $conf_print = (isset($_GET["t"]) && !empty($_GET["t"]))?$_GET["t"]:"";
                //recuperando informe por id
                $informe = $reportecontable->getReporteConableById($idreporte);
                if($informe){
                    //agregando ciclo de una sola vuelta para recuperar datos de la venta
                foreach($informe as $data){}
                //ecuperando la sucursal por factura de venta
                $sucursal = $sucursales->getSucursalById($_SESSION["idsucursal"]);
                //traer la vista del tipo de impresion que se aplica a este comprobante
                $funcion = $data->rcc_type."_reporte";
                //configuracion solo para desarrollo
                //$location = "http://127.0.0.1/app";
                $location = LOCATION_CLIENT;

              $this->frameview("file/pdf/reporte",array(
                   "file_height"=>$file_height,
                   "conf_print"=>$conf_print,
                   "sucursal"=>$sucursal,
                   "funcion"=>$funcion,
                   "id"=>$data->rcc_id,
                   "url"=>$location,
                   "redirect"=>"#comprobantes/menu",
               ));
                }else{
                    echo "Factura no disponible";
                }
            }else{
                
            }
        }
    }
    public function BC_reporte(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >0){
            if(isset($_GET["data"]) && !empty($_GET["data"]) && $_GET["data"]>0){
            //models
            $pdf = new FPDF('P','mm',array(100,150),$this->adapter);
            $comprobantecontable = new ComprobanteContable($this->adapter);
            $reportecontable = new ReporteContable($this->adapter);
            //functions
            $idreporte = $_GET["data"];
            $informe = $reportecontable->getReporteConableById($idreporte);
            foreach ($informe as $informe) {}
            if($informe){
                $comprobantes = $comprobantecontable->reporte_detallado_comprobante_por_cuenta($informe->rcc_start_date,$informe->rcc_end_date,$informe->rcc_param1,$informe->rcc_param2);
                $start_last_mont =date("Y-m-d",strtotime($informe->rcc_start_date."- 1 month"));
                $end_last_mont =date("Y-m-d",strtotime($informe->rcc_end_date."- 1 month"));
                $instance =[];
                $tablehead = array("Cuenta","Saldo anterior","Debito","Credito.","Saldo final");
                $saldo_anterior =0;
                $debito=0;
                $credito=0;
                $saldo_final=0;
                foreach($comprobantes as $comprobantes){
                    //llamar la misma query pero limitando los codigos solo al actual en el array
                        $total_cuenta = $comprobantecontable->reporte_total_comprobante_por_cuenta($start_last_mont,$end_last_mont,$comprobantes->dcc_cta_item_det,$comprobantes->dcc_d_c_item_det);
                        foreach ($total_cuenta as $total_cuenta) {}
                    //echo "$".$total_cuenta->total_cuenta." ".$comprobantes->dcc_cta_item_det." D:".$comprobantes->debito." C:".$comprobantes->credito."</br>";
                    $instance[] = array(
                        $comprobantes->dcc_cta_item_det,
                        number_format($total_cuenta->total_cuenta,2,'.',','),
                        number_format($comprobantes->debito,2,'.',','),
                        number_format($comprobantes->credito,2,'.',','),
                        number_format(($total_cuenta->total_cuenta+$comprobantes->debito)-$comprobantes->credito,2,'.',',')
                    );
                    $saldo_anterior += $total_cuenta->total_cuenta;
                    $debito +=$comprobantes->debito;
                    $credito +=$comprobantes->credito;
                    $saldo_final += ($total_cuenta->total_cuenta+$comprobantes->debito)-$comprobantes->credito;
                }
                $instance[] =array(
                    "",
                    number_format($saldo_anterior,2,'.',','),
                    number_format($debito,2,'.',','),
                    number_format($credito,2,'.',','),
                    number_format($saldo_final,2,'.',',')
                );
                $pdf->SetTitle("Registro de comprobante ");
                $array =array(
                    "tipo_doc"=>"REPORTE ",
                    "comprobante"=>"BLCE. COMPROBACION",
                );
                $pdf->setData($array);
                $pdf->AddPage('P','A4','');
                $pdf->SetY(80);
                $pdf->SetX(16);
                $pdf->FancyTableReporte($tablehead,$instance);

                $pdf->AutoPrint();
                $pdf->output();
            }else{
                echo "Este reporte no es valido";
            }
            //
            }
        }
    }

    public function MC_reporte(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >0){
            if(isset($_GET["data"]) && !empty($_GET["data"]) && $_GET["data"]>0){
            //models
            $pdf = new FPDF('P','mm',array(100,150),$this->adapter);
            $comprobantecontable = new ComprobanteContable($this->adapter);
            $reportecontable = new ReporteContable($this->adapter);
            //functions
            $idreporte = $_GET["data"];
            $informe = $reportecontable->getReporteConableById($idreporte);
            foreach ($informe as $informe) {}
            if($informe){
                
                $comprobantes = $comprobantecontable->reporte_detallado_comprobante_por_cuenta($informe->rcc_start_date,$informe->rcc_end_date,$informe->rcc_param1,$informe->rcc_param2);
                $start_last_mont =date("Y-m-d",strtotime($informe->rcc_start_date."- 1 month"));
                $end_last_mont =date("Y-m-d",strtotime($informe->rcc_end_date."- 1 month"));
                $instance =[];
                $tablehead = array("Cuenta","Comprobante","Consecutivo","Debitos.","Creditos");
                $debito=0;
                $credito=0;
                $totalinstance=[];
                foreach($comprobantes as $comprobantes){
                    $subinstance=[];
                    //llamar la misma query pero limitando los codigos solo al actual en el array
                        $total_cuenta = $comprobantecontable->reporte_total_comprobante_por_cuenta($start_last_mont,$end_last_mont,$comprobantes->dcc_cta_item_det,$comprobantes->dcc_d_c_item_det);
                        foreach ($total_cuenta as $total_cuenta) {}
                        //recuperar todas los comprobantes con esta cuenta
                            $registros = $comprobantecontable->reporte_general_comprobante_por_cuenta($informe->rcc_start_date,$informe->rcc_end_date,$comprobantes->dcc_cta_item_det);
                            foreach ($registros as $registro) {
                                $subinstance[]=array(
                                    $registro->cc_num_cpte,
                                    $registro->cc_cons_cpte,
                                    $registro->debito,
                                    $registro->credito
                                );
                                $debito +=$registro->debito;
                                $credito +=$registro->credito;
                            }

                        $instance[]= array(
                            $comprobantes->dcc_cta_item_det,
                            $subinstance
                        );

                }

                $pdf->SetTitle("Registro de comprobante ");
                $array =array(
                    "tipo_doc"=>"REPORTE ",
                    "comprobante"=>"BLCE. COMPROBACION",
                );
                $pdf->setData($array);
                $pdf->AddPage('P','A4','');
                $pdf->SetY(80);
                $pdf->SetX(16);
            $pdf->FancyTableReporte2($tablehead,$instance);
            $pdf->AutoPrint();
            $pdf->output();

            }else{echo "Este reporte no es valido";}
            }else{}
        }else{}
    }

    function XLS_reporte(){
        if(isset($_SESSION["idsucursal"]) && !empty($_SESSION["idsucursal"]) && $_SESSION["permission"] >2){
            if(isset($_GET["data"]) && !empty($_GET["data"])){
                $reportecontable = new ReporteContable($this->adapter);
                //functions
                $idreporte = $_GET["data"];
                $informe = $reportecontable->getReporteConableById($idreporte);
                foreach ($informe as $informe) {}
            if($informe){
                
                require_once 'lib/PHPExcel/PHPExcel.php';
                //models
                $excel = new PHPExcel(); 
                $articulo = new Articulo($this->adapter);
                $compra = new Compras($this->adapter);
                $detallecompra = new DetalleIngreso($this->adapter);
                $venta = new Ventas($this->adapter);
                $detalleventa = new DetalleVenta($this->adapter);
                //functions
                
                $column_start =6;
                $listCompra=[];
                $detailCompra=[];
                $detailVenta=[];
                $detailKardex=[];
                $detailSaldo=[];
                $start_date = $informe->rcc_start_date;
                $end_date = $informe->rcc_end_date;
                $idarticulo =$informe->rcc_param1;
                $column_used=[];
                $folder_master = "files/xls/";
                $filename = "KARDEX S".$_SESSION["idsucursal"]." ".$start_date." - ".$end_date." ".$idarticulo."-".$idreporte;
                $compras = $compra->getDetalleComprasByDay($start_date,$end_date,'a.idarticulo',$idarticulo);
                $ventas = $venta->getDetalleVentasByDay($start_date,$end_date,'a.idarticulo',$idarticulo);
                $comprasAll = $detallecompra->getDetalleAllByDay();
                $ventasAll = $detalleventa->getDetalleAll();
                $articulos = $articulo->getArticuloById($idarticulo); 
                foreach($articulos as $articulos){}

                //calculos extras
                    $total_stock_compra =0;
                    $total_precio_compra =0;
                    $promedio_precio_compra_actual =0; 
                    foreach ($compras as $calc_compra) {
                        $total_stock_compra += $calc_compra->stock_total_compras;
                        $total_precio_compra += $calc_compra->precio_total_compras;
                        $promedio_precio_compra_actual += ($calc_compra->precio_total_compras /$calc_compra->stock_total_compras);
                    }
                    $detailCompra[] = array(
                        $total_stock_compra,
                        $total_precio_compra,
                    );
                    $total_stock_venta =0;
                    $total_precio_venta =0;
                    foreach ($ventas as $calc_venta) {
                        $total_stock_venta +=  $calc_venta->stock_total_ventas;
                        $total_precio_venta += $calc_venta->precio_total_ventas;
                    }
                    $detailVenta[] =array(
                        $total_stock_venta,
                        $total_precio_venta,
                    );


                    ################## saldo anterior
                    $mes_anterior = date("m",strtotime($start_date)) - 1;
                    $stock_anterior = 0;
                    $precio_compra_anterior = 0;
                    $promedio_precio_compra_anterior =0;
                    $precio_venta_anterior =0;
                    
                    foreach ($comprasAll as $compra_anterior){
                        $mes_compra = date("m",strtotime($compra_anterior->fecha));
                        if($mes_compra == $mes_anterior && $compra_anterior->idarticulo == $idarticulo){
                            $stock_anterior += $compra_anterior->stock_total_compras;
                            $precio_compra_anterior  += $compra_anterior->precio_total_lote; ##junto con impuesto
                        }
                    }
                    $promedio_precio_compra_anterior = ($stock_anterior)?$precio_compra_anterior / $stock_anterior:0;


                    foreach ($ventasAll as $venta_anterior){
                        $mes_venta = date("m",strtotime($venta_anterior->fecha));
                        
                        if($mes_venta == $mes_anterior && $venta_anterior->idarticulo == $idarticulo){
                            $stock_anterior -= $venta_anterior->cantidad;
                            $precio_venta_anterior += $venta_anterior->precio_total_lote;
                            $precio_compra_anterior -= $promedio_precio_compra_anterior;
                            
                        }
                    }
                    $salida_consecutiva = $total_stock_compra + $stock_anterior;
                    $precio_anterior_descendiendo = $precio_compra_anterior;


                    foreach ($ventas as $calc_venta2) {
                        $salida_consecutiva -= $calc_venta2->stock_total_ventas;
                        $detailSaldo[]=array(  
                            $salida_consecutiva,
                            $precio_compra_anterior + $calc_venta2->stock_total_ventas ,
                        );
                    }
                //Usamos el worsheet por defecto 
                $sheet = $excel->getActiveSheet(); 
        
                $header= array(
                    "A5"=>"Fecha",
                    "B5"=>"Comprobante",
                    "C5"=>"CC/NIT",
                    "D5"=>"Nombre",
                    "E5"=>"Cantidad",
                    "F5"=>"Costo/Precio",
                    "G5"=>"Total",
                );
                //set header properties
                $properties = [];
                $properties[] = array("A2:G2","267084","FFFFFF");
                $properties[] = array("A3:G3","267084","FFFFFF");
                $properties[] = array("A4:G4","267084","FFFFFF");
                //HEADER
                $sheet->setCellValue('A3', "Fecha inicio");
                $sheet->setCellValue('A4', $start_date);
                $sheet->setCellValue('C3', "Fecha fin");
                $sheet->setCellValue('C4', $end_date);
                $sheet->setCellValue('D3', "Articulo");
                $sheet->setCellValue('E3', $articulos->nombre_articulo);

                $sheet->setCellValue('E4', "Saldo anterior: $stock_anterior");
                $sheet->setCellValue('F4', "$promedio_precio_compra_anterior");
                $sheet->setCellValue('G4', "$precio_compra_anterior");
                
                
                foreach($header as $header => $value){
                    
                    
                    $sheet->setCellValue("$header", "$value"); 
                    $sheet->getStyle("$header")->getFont()->setName('Tahoma')->setBold(true)->setSize(8)->getColor()->setRGB("FFFFFF"); 
                    $sheet->getStyle("$header")->getBorders()->applyFromArray(array('allBorders' => 'thin')); 
                    $sheet->getStyle("$header")->getAlignment()->setVertical('center')->setHorizontal('center');
                    $excel->getActiveSheet()
                    ->getStyle("$header")
                    ->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('4691a5');
                    $excel->getActiveSheet()->getColumnDimension(substr($header,0,1))->setWidth(20);
                }


        
                //setear propiedades a la cabecera
                $compra_start_at = $column_start;
                foreach ($compras as $compra) {
                    $promedio_total_compra =$compra->precio_total_compras / $compra->stock_total;
                    $sheet->setCellValue('A'.$compra_start_at, $compra->fecha);
                    $sheet->setCellValue('B'.$compra_start_at, $compra->serie_comprobante."".zero_fill($compra->num_comprobante,8)); 
                    $sheet->setCellValue('C'.$compra_start_at, $compra->documento_tercero); 
                    $sheet->setCellValue('D'.$compra_start_at, $compra->nombre_proveedor); 
                    $sheet->setCellValue('E'.$compra_start_at, $compra->stock_total); 
                    $sheet->setCellValue('F'.$compra_start_at, "$promedio_total_compra"); 
                    $sheet->setCellValue('G'.$compra_start_at, $compra->precio_total_compras); 
                    $sheet->getStyle('A'.$compra_start_at.":G".$compra_start_at)->getAlignment()->setVertical('center')->setHorizontal('right');
                    $compra_start_at ++;
                }
                $column_used[]=$compra_start_at;
                
                $ventas_start_at =$compra_start_at;
                foreach ($ventas as $venta) {
                    if($venta){
                        if(isset($venta->fecha) && !empty($venta->fecha)){
                            $precio_total_ventas = -abs($venta->precio_total_ventas);
                            $stock_total = -abs($venta->stock_total);
                            $promedio_total = -abs($venta->precio_total_ventas / $venta->stock_total);
                            $sheet->setCellValue('A'.$ventas_start_at, $venta->fecha);
                            $sheet->setCellValue('B'.$ventas_start_at, $venta->serie_comprobante."".zero_fill($venta->num_comprobante,8)); 
                            $sheet->setCellValue('C'.$ventas_start_at, $venta->documento_tercero);
                            $sheet->setCellValue('D'.$ventas_start_at, $venta->nombre_cliente);
                            $sheet->setCellValue('E'.$ventas_start_at, "$stock_total");
                            $sheet->setCellValue('F'.$ventas_start_at, "$promedio_total");
                            $sheet->setCellValue('G'.$ventas_start_at, "$precio_total_ventas");
                            $sheet->getStyle('A'.$ventas_start_at.":G".$ventas_start_at)->getAlignment()->setVertical('center')->setHorizontal('right');
                            $ventas_start_at ++;
                        }
                    }
                }
                $column_used[]=$ventas_start_at;

                // $saldo_start_at=$column_start;
                // foreach($detailSaldo as $listadoSaldo){
                //     $sheet->setCellValue('G'.$saldo_start_at, "$listadoSaldo[0]");
                //     $sheet->setCellValue('H'.$saldo_start_at, "$listadoSaldo[1]"); 
                //     $calc = number_format($listadoSaldo[1]/$listadoSaldo[0],2,'.',',');
                //     $sheet->setCellValue('I'.$saldo_start_at, "$calc"); 
                //     $sheet->getStyle('G'.$saldo_start_at.":I".$saldo_start_at)->getAlignment()->setVertical('center')->setHorizontal('right');
                //     $saldo_start_at ++;
                // }
                // $column_used[]=$saldo_start_at;
                
                foreach($column_used as $column_use){
                    $properties[] = array("A".$column_start.":G".($column_use-1),"5baabf","FFFFFF");
                }
                //descubrir el maximo de columnas usadas en vertical desde la cabecera de la tabla
                $max_column = max($column_used);

                //imprimir totales dependiendo de la fila
                $sheet->setCellValue('E'.$max_column, "=SUM(E".$column_start.":E".($max_column-1).")");
                $excel->getActiveSheet()->getStyle('E'.$max_column)->getFont()->setBold(true);
                $sheet->setCellValue('F'.$max_column, "=SUM(F".$column_start.":F".($max_column-1).")");
                $excel->getActiveSheet()->getStyle('F'.$max_column)->getFont()->setBold(true);
                $sheet->setCellValue('G'.$max_column, "=SUM(G".$column_start.":G".($max_column-1).")");
                $excel->getActiveSheet()->getStyle('G'.$max_column)->getFont()->setBold(true);
                $properties[]=array("A".$max_column.":G".$max_column,"79cbe1","FFFFFF");
                //do properties
                foreach($properties as $propertie){
                    $excel->getActiveSheet()
                    ->getStyle("$propertie[0]")
                    ->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB("$propertie[1]");
                    $excel->getActiveSheet()->getStyle("$propertie[0]")->getFont()->setBold(false)->getColor()->setRGB("$propertie[2]");
                }
        
                $writer = new PHPExcel_Writer_Excel5($excel); 
                $writer->save($folder_master.$filename.'.xls');
                $file_height = "95.5%";
                $location = $folder_master.$filename.'.xls';
                $this->frameview("file/xls/Excel",array(
                    "file_height"=>$file_height,
                    "url"=>$location,
                    "redirect"=>"#comprobantes/menu",
                ));
            }
        }

        }
    }

    function informe_kardex(){
        

    }

}