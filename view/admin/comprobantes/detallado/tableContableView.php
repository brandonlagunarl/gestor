<?php foreach ($detalle as $detalle) {
    $debito = ($detalle->dcc_d_c_item_det == "D")?$detalle->dcc_valor_item:0;
    $credito = ($detalle->dcc_d_c_item_det == "C")?$detalle->dcc_valor_item:0;

    if($detalle->cc_estado == "A" ){
      $estado = "fa-check-circle";
      $color = "text-success";
      $message ="Aceptado";
  }elseif($detalle->cc_estado =="A" && !$detalle->dcc_cta_item_det){
      $estado = "fa-exclamation-circle";
      $color = "text-warning";
      $message ="Alerta";
  }
  else{
      $estado = "fa-times-circle";
      $color = "text-dange";
      $message ="Cancelado";
  }

  ?>
              <tr>
                  <td><?=$detalle->cc_fecha_cpte?></td>
                  <td><?=$detalle->nombre_tercero?></td>
                  <td><?=$detalle->prefijo." ".$detalle->cc_num_cpte."".zero_fill($detalle->cc_cons_cpte,8)?></td>
                  <td><?=$detalle->idcodigo?></td>
                  <td><?=number_format($debito,2,'.',',')?></td>
                  <td><?=number_format($credito,2,'.',',')?></td>
                  <td><i class="fas <?=$estado." "?> <?=$color?>" data-toggle="tooltip-primary" data-placement="top" title="Estado <?=$message?>"></i></td>
              </tr>
            <?php } ?>